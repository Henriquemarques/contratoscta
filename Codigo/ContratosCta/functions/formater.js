exports.siglaestado = (estado) => {
    let sigla = ''
    switch (estado) {
        case "São Paulo":
            sigla = 'SP'
            break;
        case "Rio de Janeiro":
            sigla = 'RJ'
            break;
        case "Espírito Santo":
            sigla = 'ES'
            break;
        case "Acre":
            sigla = 'AC'
            break;
        case "Alagoas":
            sigla = 'AL'
            break;
        case "Amapá":
            sigla = 'AP'
            break;
        case "Amazonas":
            sigla = 'AM'
            break;
        case "Bahia":
            sigla = 'BA'
            break;
        case "Ceará":
            sigla = 'CE'
            break;
        case "Goiás":
            sigla = 'GO'
            break;
        case "Maranhão":
            sigla = 'MA'
            break;
        case "Mato Grosso":
            sigla = 'MT'
            break;
        case "Mato Grosso do Sul":
            sigla = 'MS'
            break;
        case "Minas Gerais":
            sigla = 'MG'
            break;
        case "Paraíba":
            sigla = 'PB'
            break;
        case "Pará":
            sigla = 'PA'
            break;
        case "Paraná":
            sigla = 'PR'
            break;
        case "Pernambuco":
            sigla = 'PE'
            break;
        case "Piauí":
            sigla = 'PI'
            break;
        case "Rio Grande do Norte":
            sigla = 'RN'
            break;
        case "Rio Grande do Sul":
            sigla = 'RS'
            break;
        case "Rondônia":
            sigla = 'RO'
            break;
        case "Roraima":
            sigla = 'RR'
            break;
        case "Santa Catarina":
            sigla = 'SC'
            break;
        case "Sergipe":
            sigla = 'SE'
            break;
        case "Tocantins":
            sigla = 'TO'
            break;
        case "Distrito Federal":
            sigla = 'Distrito Federal'
            break;
    }
    return sigla
}

exports.formataTel = (rg) => {
    rg = rg.replace(/\D/g, "");
    rg = rg.replace(/(\d{2})(\d{4,5})(\d{4})$/, "($1)$2-$3");


    return rg
}


exports.formataCPF = (cpf) => {

    cpf = cpf.replace(/[^\d]/g, "");

    return cpf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");
}

exports.getLongDate = () => {
    const currentDate = new Date()
    const date = currentDate.getDate();
    const month = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"][currentDate.getMonth()];
    const year = currentDate.getFullYear();
    return `${date} de ${month} de ${year}`;
}


exports.getShortDate = (dateToFormat) => {
    let currentDate = dateToFormat;
    if (!dateToFormat) currentDate = new Date();
    const day = currentDate.getDate();
    const year = currentDate.getFullYear();
    return day.toString().padStart(2, '0') + "/" + (currentDate.getMonth() + 1).toString().padStart(2, '0') + "/" + year;
}

exports.capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

