const functions = require("firebase-functions");
const express = require('express');
const bodyParser = require('body-parser')
const app = express();
const lib = require('pipedrive');
const gerardocumento = require('./gerardocs').gerardocumento
require('dotenv').config()
fs = require('fs');
app.use(bodyParser.json())


// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions


exports.pdfs = functions.https.onRequest(async (request, response) => {
  //console.log(request.body)
  const host = gethost(request.body.meta.host)
  const ishostValid = await checkhost(host, response)
  if (ishostValid) {
    console.log("Host invalido")
    return response.send()
  }
  lib.Configuration.apiToken = process.env[host.concat('_APIKEY')]
  console.log("Chave de acesso para APIPipe: " + process.env[host.concat('_APIKEY')])
  console.log("Tentativas = " + request.body.retry)
  
  const MultipleRetrys = await checkretry(request.body.retry, response, host)
  if (MultipleRetrys) {
    return response.send()
  }

  switch (request.body.current.stage_id) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
      return response.status(200).send()

    case 6:
      const pessoa = await lib.PersonsController.getDetailsOfAPerson(request.body.current.person_id)

      const deal = await lib.DealsController.getDetailsOfADeal(request.body.current.id)

      console.log("Sem erro de tentativas")

      const sucesso = await gerardocumento(pessoa, deal, request, host)
      if (sucesso) {
        console.log("Tudo certo")
        response.send()
        break
      }
      else {
        console.log("Deu erro")
        response.send()
        break
      }


    case 7:
      return response.send();

    default:
      return response.send();
  }
});

function gethost(rawhost) {
  rawhost = rawhost.replace(".pipedrive.com", '').toUpperCase()
  return rawhost
}

async function checkhost(host) {
  console.log(host)

  if (host === 'APP') {
    return true
  }
  return false
}


async function checkretry(retry, response) {
  if (retry >= 1) {
    console.log("Erro de Multiplas Tentativas")
    return true
  }
  return false
}

