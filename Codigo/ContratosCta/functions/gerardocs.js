const express = require('express');
const bodyParser = require('body-parser')
const app = express();
const lib = require('pipedrive');
const fetch = require('node-fetch')
const docs = require('./docs.js');
const Extenso = require('./extenso.js').Extenso
const siglaestado = require('./formater').siglaestado
const formataTel = require('./formater').formataTel
const formataCPF = require('./formater').formataCPF
const getLongDate = require('./formater').getLongDate
const capitalizeFirstLetter = require('./formater').capitalizeFirstLetter
const path = require('path');
const FormData = require('form-data');
require('dotenv').config()
fs = require('fs');
app.use(bodyParser.json())


exports.gerardocumento = async (pessoa, deal, request, host) => {

    const info = {
        "Nome": pessoa.data.name,
        "Email": pessoa.data.email[0].value,
        "Telefone": pessoa.data.phone[0].value,
        "Endereço": pessoa.data[process.env[host.concat('_ENDERECO')]],
        "CEP": pessoa.data[process.env[host.concat('_CEP')]],
        "CPF": pessoa.data[process.env[host.concat('_CPF')]],
        "RG": pessoa.data[process.env[host.concat('_RG')]],
        "CRO": pessoa.data[process.env[host.concat('_CRO')]],
        "Valor": deal.data.value,
        "ValorExtenso": capitalizeFirstLetter(Extenso(deal.data.value, "real", "reais", "centavo", "centavos")),
        "Estado": pessoa.data[process.env[host.concat('_ESTADO')]],
        "Datalonga": getLongDate()
    }

    const isValidData = await checkinformations(info, deal)
    if (isValidData) {
        console.log("Erro: Falta de infos")
        return false
    }
    const cursos = await getcursos(deal.data.id)

    info.Curso = cursos.data[0].name

    console.log("Nome: " + info.Nome)
    console.log("Email: " + info.Email)
    console.log("Telefone: " + info.Telefone)
    console.log("Endereço: " + info.Endereço)
    console.log("CEP: " + info.CEP)
    console.log("CPF: " + info.CPF)
    console.log("RG: " + info.RG)
    console.log("CRO: " + info.CRO)
    console.log("Valor: " + info.Valor)
    console.log("Valor extenso: " + info.ValorExtenso)
    console.log("Curso: " + info.Curso)
    console.log("Estado: " + info.Estado)
    console.log("Data: " + info.Datalonga)

    if (Checkstage(request)) {
        console.log("NAO FALTA")
        const input = []
        input['content'] = "Pdf Gerado"
        input['dealId'] = deal.data.id
        lib.NotesController.addANote(input)

        await docs.gerardocs(info.Nome, info.Email, formataTel(info.Telefone), info.Endereço, info.CEP, formataCPF(info.CPF), info.RG, info.CRO, info.Valor, info.ValorExtenso, info.Curso, siglaestado(info.Estado), info.Datalonga)

        console.log("Enviando o email");

        enviararquivo(info.Email, info.Nome, deal.data.cc_email)

        return true;
    }
    else {
        console.log("roro")
        return false

    }
}



async function checkinformations(info, deal) {
    const emptyKeys = Object.keys(info).filter(el => {
        if (!info[el]) return el;
    });
    const hasProducts = deal.data.products_count > 0;
    console.log("TEM PRODUTO ?: ", hasProducts)
    const hasEmptyKeys = emptyKeys.length > 0;
    console.log("FALTA DE INFORMAÇÔES: ", hasEmptyKeys)
    const msg = `Faltam Informações: <br> ${(hasEmptyKeys) ? emptyKeys.join(", ") : ""} ${(!hasProducts) ? "E PRODUTOS" : ""}`;
    if (hasEmptyKeys || !hasProducts) {
        await updatedeal(deal.data.id, msg)
        return true;
    }
    else {
        return false;
    }
}

async function getcursos(iddeal) {
    const curso = []
    curso['id'] = iddeal
    const cursos = await lib.DealsController.listProductsAttachedToADeal(curso)
    return cursos
}

async function updatedeal(iddeal, msg) {
    const input = []
    input['stageId'] = 7
    input['id'] = iddeal
    await lib.DealsController.updateADeal(input)
    const notas = []
    notas['content'] = msg;
    notas['dealId'] = iddeal
    await lib.NotesController.addANote(notas)
    return console.log("------------------------EXISTE FALTA DE INFOS----------------------")

}
function Checkstage(request) {
    if (request.body.current.stage_id != request.body.previous.stage_id) {
        return true
    }
    else { return false }
}

async function enviararquivo(email, nome, emaildeal) {

    const form = new FormData();

    const buffer = fs.readFileSync(`/tmp/contrato.docx`);
    const fileName = `/tmp/contrato.docx`;

    form.append('file', buffer, {
        contentType: 'text/plain',
        name: 'file',
        filename: fileName,
    });
    form.append('email', email)
    form.append('nome', nome)
    form.append('emaildeal', emaildeal)


    await fetch('https://hooks.zapier.com/hooks/catch/9363767/oxtu8fw/', { method: 'POST', body: form })

        .then(res => res.json())
        .then(json => console.log(json));

}
